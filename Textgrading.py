#Text grading


import spacy
from spellchecker import SpellChecker
import tkinter
from tkinter import *
import tkinter.font as TkFont
from tkinter import messagebox

#_______main window_______

mainWindow=tkinter.Tk() 
mainWindow.title("Text grading")
mainWindow.geometry("600x400")


#_______labels_______

label1 = Label(mainWindow, text="Text Grading", bd="0", fg="black", height="3",
               width="15", font=("fotre", 15))
label1.place(relx=0.5, rely=0.1, anchor=CENTER)
label2 = Label(mainWindow, text="Insert text in the box and run", bd="0", fg="black",
               height="2", width="46", font=("forte", 12))
label2.place(relx=0.5, rely=0.3, anchor=CENTER)


#_______entry________
entry1 = Entry(mainWindow, bd="0", width="80")
entry1.place(relx=0.5, rely=0.5, anchor=CENTER)



#_______functions_________

class TextGrade:

    def __init__(self, text):
        self.text = text
    
    def run(self):
        nlp = spacy.load("en_core_web_sm")
        doc = nlp(self.text)
        words = []
        spell = SpellChecker()
        score = 10

        for token in doc:               
            words.append(''+token.text+'')
        #print(words)

        misspelled = spell.unknown(words)
        if len(misspelled) != 0:
            score = score - 1
            if len(misspelled) > 3:
                score = score - 1
            elif len(misspelled) > 5:
                score = score - 2

        if not words[0].istitle():
            score = score - 1

        if not ((self.text).endswith(".") or (self.text).endswith("!") or
                (self.text).endswith("?")):
            score = score - 1

        print(score)

        result = "Score is " + str(score)

        tkinter.messagebox.showinfo(title = "Result", message = result)

def Exit():
    if messagebox.askokcancel("Quit", "Are you sure"):
        mainWindow.destroy()
        exit(0)

def main():
    text = entry1.get()
    textObj = TextGrade(text)
    textObj.run()


#_______buttons__________
        
button1 = Button(mainWindow, text="Run", bg="white", fg="black", height="1", width="5",
                 bd="0", font=("forte", 15), command=main)
button1.place(relx=0.5, rely=0.7, anchor=CENTER)
button2 = Button(mainWindow, text="Exit", bg="white", fg="red", height="3", width="12",
                 bd="0", font=("forte", 15), command=Exit).place(relx=0.5, rely=0.9, anchor=CENTER)




mainWindow.mainloop()
    


    
