# Text Grading
#Coded by Ujwal Rajeev, Vyshak A, Teena paulose, Swaliha AbdulSalam

Grading a text based on quality from 1 - 10

Please install the required libraries before running the program
1. spacy - https://spacy.io/
2. spellchecker - https://pypi.org/project/pyspellchecker/
3. Tkinter - https://docs.python.org/3/library/tkinter.html

